# cloudformation-example

En este ejemplo veremos el uso de cloudformation para desplegar una infraestructura que de soporte a un wordpress con autoscaling group.
El stack esta compuesto de
- 1 VPC (Red Privada Virtual)
- 1 ELB (Balanceador de carga)
- 3 Subredes, 2 publicas y 1 privada. La 2 redes publicas son para que el balanceador de carga cree las instancias en redes diferentes (con zonas de disponibilidad diferentes)
- 3 Instancias (Un Webserver, un database server y un host bastion para que las instancias de la red privada puedan acceder a internet)
- 1 Autoscaling group que incrementara y disminuira la cantidad de instancias frontend segun la demanda.
- 1 Launch configuration para crear on-demand las instancias frontend (webserver) necesarias

En el directorio Utils, se encuentra un script que obtiene a partir de un nombre de una AMI, los IDs de dicha AMI en las diferentes regiones (es utilizado en los mappings, para garantizar que el stack sea multi-región)